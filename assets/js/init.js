$(document).ready(function () {
    $(".rotate").textrotator({
        animation: "dissolve", // You can pick the way it animates when rotating through words. Options are dissolve (default), fade, flip, flipUp, flipCube, flipCubeUp and spin.
        separator: ",", // If you don't want commas to be the separator, you can define a new separator (|, &, * etc.) by yourself using this field.
        speed: 2000 // How many milliseconds until the next word show.
    });

//code fotr smooth scrolling
    $(".updates-link").click(function () {
        $('.fourth-wrapper').ScrollTo();
    });

    //scroll function in jquery
    $(window).scroll(function () {
        if ($(window).scrollTop() > 80) {
            $(".navbar-collapse").addClass("navbar-collapse-scroll");
            $(".left-logo").addClass("left-logo-none");
            $(".short-logo").addClass("short-logo-display");
        } else {
            $(".navbar-collapse").removeClass("navbar-collapse-scroll");
            $(".short-logo").removeClass("short-logo-display");
            $(".left-logo").removeClass("left-logo-none");
            
        }
    });
    $('.image').each(function (i, obj) {
        $(this).on("mouseover", function () {
            hoverVideo(i);
        });
        $(this).on("mouseout", function () {
            hideVideo(i);
        });
    });
//    $(".image").hover(function () {
//        $(this).children(".jury-inside-text").fadeOut(200, function () {
//            $(this).parent().removeClass("bg-image");
//            $(this).siblings().show();
//        });
//    }, function () {
//        $(this).children(".bg-video").fadeOut(200, function () {
//            $(this).parent().addClass("bg-image");
//            $(this).siblings().show();
//        });
//    });
});

function hoverVideo(i) {
    $('.thevideo')[i].play();
}

function hideVideo(i) {
    $('.thevideo')[i].pause();
}