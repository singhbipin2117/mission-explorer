<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title><?php echo $title ?></title>

        <!-- Latest compiled and minified CSS -->
        <link href="<?php echo $css_path.'bootstrap.min.css';?>" rel="stylesheet" type="text/css"/>
        
        <!-- load custom css-->
        <link rel="stylesheet" href="<?php echo $css_path.'style.css'; ?>">
        <!-- jquery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <!--bootstrap min js -->
        <script src="<?php echo $js_path.'bootstrap.min.js';?>" type="text/javascript"></script>
        <script src="<?php echo $js_path.'jquery-scrollto.js'?>" type="text/javascript"></script>
        <script src="<?php echo $js_path.'jquery.simple-text-rotator.min.js';?> " type="text/javascript"></script>
        <script  src="<?php echo $js_path.'init.js'; ?>"></script>
        <link rel="stylesheet" href="<?php // echo $css_path.'font-awesome.min.css'; ?>">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>