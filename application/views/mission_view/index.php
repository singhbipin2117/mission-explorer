<!-- container div start-->
<div class="container-fluid" >
    <div class="row">
        <!-- navbar start-->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid navbar-wrapper">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand short-logo" href="#"><img src="<?php echo $image_path . "short-logo.png"; ?>"></a>
                    <a class="navbar-brand left-logo" href="#"><img src="<?php echo $image_path . "left-logo.png"; ?>"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-shrink" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#"><img src="<?php echo $image_path . "home.png"; ?>"></a></li>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Participate</a></li>
                        <li><a href="#">Jury</a></li>
                        <li><a href="#">Prize</a></li>
                        <li><a href="javascript:void(0)" class="updates-link">Updates</a></li>

                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav><!-- navbar ends here -->
        <div class="col-md-12 col-sm-12 col-xs-12 wrapper"> <!-- banner start here -->
            <div class="wrapper-inside">
                <!-- banner image -->
                <p class="wrraper-first-p">DO YOU <br> LOVE EXPLORING</p>
                <p class="wrraper-first-p color rotate">CULTURE? ,Simple, Customizable, Light Weight, Easy</p>
                <p class="wrraper-first-p fonts">National Geographic Channel India is on a mission.<br>
                    To find the people that embody its spirit of exploration.

                </p>
                <p class="wrraper-first-p fonts padding-management">And we're going to send them on an<br>
                    expedition to Machu Pichu.</p>
                <p class="button-wrapper"><img src="<?php echo $image_path . "participate-now.png"; ?>"><img src="<?php echo $image_path . "know-more.png"; ?>"></p>
            </div>
        </div> <!-- banner ends here -->
    </div>
</div> <!-- contwiner fluid ends here -->
<div class="container-fluid main-second-wrapper">
    <div class="row">
<div class="container-fluid third-wrapper-container-fluid">   
<div class="row">
<div class="container third-wrapper"> <!-- third section start here -->
    <div class="row">
        <div class="col-md-6 col-sm-12 col-xs-12 jury"> <!-- carausel start here -->
            <b class="second-text">THE JURY</b>
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img width="100%" src="<?php echo $image_path . "midival.png"; ?>">
                        <div class="carousel-caption">
                            <img src="<?php echo $image_path . "watch-video.png"; ?>">
                        </div>
                    </div>
                    <div class="item">
                        <img width="100%" src="<?php echo $image_path . "midival.png"; ?>">
                        <div class="carousel-caption">
                            <img src="<?php echo $image_path . "watch-video.png"; ?>">
                        </div>
                    </div>
                    <div class="item">
                        <img width="100%" src="<?php echo $image_path . "midival.png"; ?>">
                        <div class="carousel-caption">
                            <img src="<?php echo $image_path . "watch-video.png"; ?>">
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- carausel ends here -->

        <div class="col-md-6 col-sm-12 col-xs-12 grand-prize">
            <b class="second-text">THE GRAND PRIZE</b>
            <div class="wrap-overflow">
                <img width="100%" src="<?php echo $image_path . "win-nat-geo.png"; ?>" class="img-responsive scale-image">
            </div>
        </div>
    </div>
</div> <!-- third section ends here -->
</div>
</div>
<div class="container-fluid fourth-wrapper-container-fluid">   <!-- fourth section start here -->
    <div class="row" style="padding: 35px 0;">
        <div class="container fourth-wrapper">
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12 updates">
                    <div class="inner-content">
                        <img src="<?php echo $image_path . "face.png"; ?>">
                        <p class="tweet-handle">@abhishekmadan undertakes the<br>
                            #ExplorerChallenge.
                        </p>
                        <p class="tweet-text">
                            A quest to explore the forests inside urban spaces in Bengaluru,<br>
                            discover their diversity and the challenges they face.<br>
                            Go witness the Explorer Challenge: Bengaluru Edition.

                        </p>
                        <p> <img src="<?php echo $image_path . "click-here.png"; ?>"></p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12 twitter-widget">
                    <!-- code for twitter widget -->

                    <a class="twitter-timeline" href="https://twitter.com/inNatGeo" data-widget-id="659690073500217344">Tweets by @inNatGeo</a>
                    <script>
                        !function (d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0],
                                    p = /^http:/.test(d.location) ? 'http' : 'https';
                            if (!d.getElementById(id)) {
                                js = d.createElement(s);
                                js.id = id;
                                js.src = p + "://platform.twitter.com/widgets.js";
                                fjs.parentNode.insertBefore(js, fjs);
                            }
                        }(document, "script", "twitter-wjs");</script>
                </div>
            </div>
        </div>
    </div>

    <!--footer start here -->

    <footer class="footer">
        <div class="container">
            <p class="footer-text">2015 National Geographic Channel India. All rights reserved.    |    T&C</p>
        </div>
    </footer>

</div>  <!-- fourth section ends here -->
</div>
</div>