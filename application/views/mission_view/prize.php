<!-- container div start-->
<div class="container-fluid prize-main-container">
    <div class="row">
        <!-- navbar start-->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid navbar-wrapper">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand short-logo" href="#"><img src="<?php echo $image_path . "short-logo.png"; ?>"></a>
                    <a class="navbar-brand left-logo" href="#"><img src="<?php echo $image_path . "left-logo.png"; ?>"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-shrink" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#"><img src="<?php echo $image_path . "home.png"; ?>"></a></li>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Participate</a></li>
                        <li><a href="#">Jury</a></li>
                        <li><a href="#">Prize</a></li>
                        <li><a href="javascript:void(0)" class="updates-link">Updates</a></li>

                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav><!-- navbar ends here -->
        <div class="col-md-12 col-sm-12 col-xs-12 prize-header"> <!-- banner start here -->

        </div> <!-- banner ends here -->
    </div>
    <div class="container">
        <div class="prize-container">
            <div class='row remove-margin'>
                <div class="col-sm-6 col-xs-12 padding-4">
                    <div class="prize-copy">
                        <h1>
                            3 EXPLORERS WILL EMBARK ON a Nat Geo Expedition to Machu Pichu 
                        </h1>
                        <p>
                            A glimpse into the Incan legacy and vibrant Peruvian traditions. 
                            An 8-day journey into the legacy of the Inca and the vibrant traditions of Peruvian culture at Machu Picchu, Cusco, and the Sacred Valley of the Inca.
                            Explore the world. Explore heritage. Explore culture. Explore nature. Explore it all.  You could win the opportunity of a lifetime.
                        </p>
                        <a href='#'>
                            <img  src="<?php echo $image_path . "prize-participate.jpg"; ?>">
                        </a>
                        <div>
                            <img  class="img-responsive" src="<?php echo $image_path . "prize1-1.jpg"; ?>">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12 padding-4">
                    <img  class="img-responsive" src="<?php echo $image_path . "prize1-2.jpg"; ?>">
                </div>
            </div>
            <div class="row remove-margin">
                <div class="col-xs-12 padding-4">
                    <img  class="img-responsive" src="<?php echo $image_path . "prize2.jpg"; ?>">
                </div>
            </div>
            <div class="row remove-margin">
                <div class="col-sm-6 col-xs-12 padding-4">
                    <img  class="img-responsive" src="<?php echo $image_path . "prize3-1.jpg"; ?>">
                </div>
                <div class="col-sm-6 col-xs-12 padding-4">
                    <img class="img-responsive" src="<?php echo $image_path . "prize3-2.jpg"; ?> ">
                </div>
            </div>
        </div>

    </div>

</div> <!-- contwiner fluid ends here -->


<!--footer start here -->

<footer class="footer">
    <div class="container">
        <p class="footer-text">2015 National Geographic Channel India. All rights reserved.    |    T&C</p>
    </div>
</footer>
