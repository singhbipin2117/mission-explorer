<!-- container div start-->
<div class="container-fluid" >
    <div class="row">
        <!-- navbar start-->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid navbar-wrapper">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand left-logo" href="#"><img src="<?php echo $image_path . "left-logo.png"; ?>"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-shrink" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#"><img src="<?php echo $image_path . "home.png"; ?>"></a></li>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Participate</a></li>
                        <li><a href="#">Jury</a></li>
                        <li><a href="#">Prize</a></li>
                        <li><a href="javascript:void(0)" class="updates-link">Updates</a></li>

                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav><!-- navbar ends here -->
        <div class="col-md-12 login-wrapper"> <!-- banner start here -->
            <p class="participate-text"><img src="<?php echo $image_path . "participate-text.png"; ?>"></p>
            <div class="wrapper-inside login-center">
                <!-- banner image -->
                <p class=""><img src="<?php echo $image_path . "login-text.png"; ?>"></p>
                <p class=""><img src="<?php echo $image_path . "fb-login.png"; ?>"></p>
                <p class=""><img src="<?php echo $image_path . "google-login.png"; ?>"></p>
                <p class=""><img src="<?php echo $image_path . "login-or.png"; ?>"></p>
                <form action="#" id="login-form" class="login-form">
                    <p><input type="email" placeholder="Email" class="email" id="email"></p>
                    <p><input type="text" placeholder="Chioose Your Password" class="password" id="password"></p>
                    <p class=""><img src="<?php echo $image_path . "submit.png"; ?>"></p>
                </form>
                <p style="margin-top:-20px"><img src="<?php echo $image_path . "forgot-password.png"; ?>"></p>
            </div>
        </div> <!-- banner ends here -->
    </div>

    <!--footer start here -->

    <footer class="footer">
        <div class="container">
            <p class="footer-text">2015 National Geographic Channel India. All rights reserved.    |    T&C</p>
        </div>
    </footer>
</div> <!-- container fluid ends here -->



</div>  <!-- fourth section ends here -->