<!-- container div start-->
<div class="container-fluid prize-main-container">
    <div class="row">
        <!-- navbar start-->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid navbar-wrapper">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand short-logo" href="#"><img src="<?php echo $image_path . "short-logo.png"; ?>"></a>
                    <a class="navbar-brand left-logo" href="#"><img src="<?php echo $image_path . "left-logo.png"; ?>"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-shrink" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#"><img src="<?php echo $image_path . "home.png"; ?>"></a></li>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Participate</a></li>
                        <li><a href="#">Jury</a></li>
                        <li><a href="#">Prize</a></li>
                        <li><a href="javascript:void(0)" class="updates-link">Updates</a></li>

                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav><!-- navbar ends here -->
        <div class="col-md-12 col-sm-12 col-xs-12 thankyou-header"> <!-- banner start here -->
            <div class="thankyou-copy">
                <h1>Thank you for sharing your love for exploration with us, Kartik!</h1>
                <div> <p>Our jury will be seeing your entry, and if they think you're one of our most dedicated explorers, you could soon be on your way to explore the diverse wonders of Machu Pichu. </p>
                </div>
                <div>
                    <p>
                        You're an Explorer, now announce it.<br/>
                        Share with your friends!
                    </p>
                    <p>
                        <span>

                        </span>
                        <span>

                        </span>
                    </p>
                </div>
                <div>
                    <p>To see more about the world and what it contains,<br/>
                        head over to our pages.
                    </p>
                    <p>
                        <span>
                            <a href='#'>
                                <img src="<?php echo $image_path . "fb-icon.jpg"; ?>">
                            </a>
                        </span>
                        <span>
                            <a href='#'>
                                <img src="<?php echo $image_path . "twitter-icon.jpg"; ?>">
                            </a>
                        </span>
                    </p>
                </div>
            </div>

        </div> <!-- banner ends here -->
    </div>
</div> <!-- contwiner fluid ends here -->


<!--footer start here -->

<footer class="footer">
    <div class="container">
        <p class="footer-text">2015 National Geographic Channel India. All rights reserved.    |    T&C</p>
    </div>
</footer>
