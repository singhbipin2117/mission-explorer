<!-- container div start-->
<div class="container-fluid prize-main-container">
    <div class="row">
        <!-- navbar start-->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid navbar-wrapper">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand short-logo" href="#"><img src="<?php echo $image_path . "short-logo.png"; ?>"></a>
                    <a class="navbar-brand left-logo" href="#"><img src="<?php echo $image_path . "left-logo.png"; ?>"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-shrink" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#"><img src="<?php echo $image_path . "home.png"; ?>"></a></li>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Participate</a></li>
                        <li><a href="#">Jury</a></li>
                        <li><a href="#">Prize</a></li>
                        <li><a href="javascript:void(0)" class="updates-link">Updates</a></li>

                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav><!-- navbar ends here -->
        <div class="col-md-12 col-sm-12 col-xs-12 about-header"> <!-- banner start here -->
            <div class="about-copy">
                <h1>National Geographic
                    is looking for  people who are curious.
                    About the world, and everything it holds. </h1>
                <div> <p>We're looking for Explorers - People who pursue interests that give them knowledge about things, satisfaction about knowing them, and a passion to know more.</p>
                    <p>People who want to discover something new, make something new, share something new, who are constantly looking at the world through an explorer’s eye.</p>
                    <p>If this sounds like you, you're an Explorer.</p>
                    <p>f you're exploring anything in the field of art, music, culture, nature, photography, music, technology or more, you could win a Nat Geo Expedition to Machu Pichu.</p>
                    <p>Get set for your next adventure.</p>
                    <p>
                        <a href="#">
                            <img src="<?php echo $image_path . "prize-participate.jpg"; ?>">
                        </a>
                    </p>
                </div>

            </div>

        </div> <!-- banner ends here -->
    </div>
</div> <!-- contwiner fluid ends here -->


<!--footer start here -->

<footer class="footer">
    <div class="container">
        <p class="footer-text">2015 National Geographic Channel India. All rights reserved.    |    T&C</p>
    </div>
</footer>
