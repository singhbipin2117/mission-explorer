
<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.html">Mahindra Admin</a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Mahindra <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li class="divider"></li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">

                <li class="active">
                    <a href="home_page"><i class="fa fa-fw fa-table"></i>Report</a>
                </li>
                <li class="active">
                    <a href="gallery"><i class="fa fa-fw fa-table"></i> Gallery</a>
                </li>

            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Tables
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-table"></i>  Dashboard
                        </li>

                    </ol>
                </div>
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col-lg-12">
                    <h2>Upload Image</h2>
                    <div class="">
                        <label>Choose option</label>
                        <select class="select" id="select">
                            <option value=""><-- Select option--></option>
                            <option value="formImage">Gallery Image Upload</option>
                            <option value="formVideo">Gallery Video Upload</option>
                        </select>

                    </div>

                    <div class="table-responsive">
                        <!-- display error-->
                        <?php
                        if (is_array($error)) {
                            if (!$error['status']) {
                                echo "<p class='alert alert-danger'>" . $error['msg'] . "</p>";
                            } else {
                                echo "<p class='alert alert-success'>" . $error['msg'] . "</p>";
                            }
                        }
                        ?>
                        <!-- form for uploading image -->
                        <form class="formImage" id="formImage" action='<?php echo base_url("index.php/login/do_upload"); ?>' enctype="multipart/form-data" method="post" accept-charset="utf-8">
                            <p><label>Upload Image for gallery</label><input type="file" name="userfile" size="20"></p>
                            <input type="submit" name="image_submit" value="Upload Image" class="btn btn-success">
                        </form>

                        <!-- form for uploading video-->
                        <form class="formVideo" id="formVideo" action='<?php echo base_url("index.php/login/do_upload"); ?>' enctype="multipart/form-data" method="post" accept-charset="utf-8">
                            <p><label>Add link for video gallery</label><input type="ulr" name="url" class="url" id="url"/>
                                <input type="file" name="userfile"/></p>
                            <input type="submit" name="video_submit" value="Upload Video Link" class="btn btn-success"/>
                        </form>
                    </div>
                </div>

            </div>
            <!-- /.row -->


        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->