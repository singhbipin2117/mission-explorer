<!-- container div start-->
<div class="container-fluid" style="background: #eeeded;overflow-x: hidden;">
    <div class="row">
        <!-- navbar start-->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid navbar-wrapper">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand left-logo" href="#"><img src="<?php echo $image_path . "left-logo.png"; ?>"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-shrink" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#"><img src="<?php echo $image_path . "home.png"; ?>"></a></li>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Participate</a></li>
                        <li><a href="#">Jury</a></li>
                        <li><a href="#">Prize</a></li>
                        <li><a href="javascript:void(0)" class="updates-link">Updates</a></li>

                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav><!-- navbar ends here -->

        <div class="container">
            <div class="row">
                <div class="col-md-12 jury-wrapper"> <!-- banner start here -->
                    <div class="jury-wrapper-inside">
                        <div class="image bg-image"><!-- bg image div start here -->
                            <div class="bg-video">
                                <video loop class="thevideo">
                                    <source src="../../assets/images/video/1.mp4" type="video/mp4">
                                    <source src="mov_bbb.ogg" type="video/ogg">
                                    Your browser does not support HTML5 video.
                                </video>
                            </div>
<!--                            <div class="jury-inside-text">
                                <p>Ishita Malviya</p>
                                <b>Exploring the swell of the waves</b>
                                <img src="<?php echo $image_path . "play.png"; ?>" class="img-responsive">
                            </div>-->
                        </div><!-- bg image div end here -->
                        <div class="image bg-image"><!-- bg image div start here -->
                            <div class="bg-video">
                                <video loop class="thevideo">
                                    <source src="../../assets/images/video/1.mp4" type="video/mp4">
                                    <source src="mov_bbb.ogg" type="video/ogg">
                                    Your browser does not support HTML5 video.
                                </video>
                            </div>
<!--                            <div class="jury-inside-text">
                                <p>Ishita Malviya</p>
                                <b>Exploring the swell of the waves</b>
                                <img src="<?php echo $image_path . "play.png"; ?>" class="img-responsive">
                            </div>-->
                        </div><!-- bg image div end here -->
                        <div class="image bg-image"><!-- bg image div start here -->
                            <div class="bg-video">
                                <video loop class="thevideo">
                                    <source src="../../assets/images/video/1.mp4" type="video/mp4">
                                    <source src="mov_bbb.ogg" type="video/ogg">
                                    Your browser does not support HTML5 video.
                                </video>
                            </div>
<!--                            <div class="jury-inside-text">
                                <p>Ishita Malviya</p>
                                <b>Exploring the swell of the waves</b>
                                <img src="<?php echo $image_path . "play.png"; ?>" class="img-responsive">
                            </div>-->
                        </div><!-- bg image div end here -->
                        <div class="image bg-image"><!-- bg image div start here -->
                            <div class="bg-video">
                                <video loop class="thevideo">
                                    <source src="../../assets/images/video/1.mp4" type="video/mp4">
                                    <source src="mov_bbb.ogg" type="video/ogg">
                                    Your browser does not support HTML5 video.
                                </video>
                            </div>
<!--                            <div class="jury-inside-text">
                                <p>Ishita Malviya</p>
                                <b>Exploring the swell of the waves</b>
                                <img src="<?php echo $image_path . "play.png"; ?>" class="img-responsive">
                            </div>-->
                        </div><!-- bg image div end here -->
                    </div>
                </div> <!-- banner ends here -->
            </div></div>
    </div>
</div> <!-- contwiner fluid ends here -->
<!--footer start here -->

<footer class="footer">
    <div class="container">
        <p class="footer-text">2015 National Geographic Channel India. All rights reserved.    |    T&C</p>
    </div>
</footer>

</div>  <!-- fourth section ends here -->