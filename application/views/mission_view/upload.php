<!-- container div start-->
<div class="container-fluid" >
    <div class="row">
        <!-- navbar start-->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid navbar-wrapper">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand short-logo" href="#"><img src="<?php echo $image_path . "short-logo.png"; ?>"></a>
                    <a class="navbar-brand left-logo" href="#"><img src="<?php echo $image_path . "left-logo.png"; ?>"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-shrink" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#"><img src="<?php echo $image_path . "home.png"; ?>"></a></li>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Participate</a></li>
                        <li><a href="#">Jury</a></li>
                        <li><a href="#">Prize</a></li>
                        <li><a href="javascript:void(0)" class="updates-link">Updates</a></li>

                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav><!-- navbar ends here -->
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 image-wrapper"> <!-- banner start here -->
            <div class="image-wrapper-inside form-body">
                <!-- banner image -->
                <p><b>If you're curious about the world and the many wonders it contains, we love the way you think. <br>
                        Tell us what you're exploring below and you could win a National Geographic Expedition to Machu Pichu!</b></p>
                <hr>
                <p><textarea class="" id="" placeholder="What did you explore?"></textarea>
                    <span><label class="ch-limit">(character limit - 50)</label></span></p>
                <p><textarea   placeholder="Describe it!"></textarea>
                    <span><label class="ch-limit">(character limit - 500 )</label></span></p>
                <p>YOU CAN ALSO</p>
                <div class="row">
                    <div class="col-sm-5 img-upload">
                        <span class="image-upload"><input type="file" name="file[]" id="file" class="file"> <img src="<?php echo $image_path . "add-btn.png"; ?>">
                        </span>
                        <p>( File format: .jpg , File size not to exceed 1MB )</p>
                    </div>
                    <div class="col-sm-7">
                       <span class="image-upload"><input type="url" name="video-link[]" id="video-link" class="video-link"> <img src="<?php echo $image_path . "add-btn.png"; ?>">
                        </span>
                        <p> ( From YouTube, Vimeo or any other video platform )</p>
                    </div>     
                </div>


                <p style="padding-top: 90px;"><input type="checkbox"><label class="condition-label"> I've read and understood all Terms & Conditions of Mission Explorer!</label></p>
                <p><input type="checkbox"><label class="condition-label"> I've read and understood all Terms & Conditions of Mission Explorer!</label></p>
                <p><input type="submit" class="submit" value="" id="submit"></p>
            </div>
        </div> <!-- banner ends here -->
    </div>

    <footer class="footer">
        <div class="container">
            <p class="footer-text">2015 National Geographic Channel India. All rights reserved.    |    T&C</p>
        </div>
    </footer>
</div> <!-- contwiner fluid ends here -->




