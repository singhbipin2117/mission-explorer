
<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.html">Mahindra Admin</a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Mahindra <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li class="divider"></li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">

                <li class="active">
                    <a href="home_page"><i class="fa fa-fw fa-table"></i>Report</a>
                </li>
                <li class="active">
                    <a href="gallery"><i class="fa fa-fw fa-table"></i> Gallery</a>
                </li>

            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Tables
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-table"></i>  Dashboard
                        </li>

                    </ol>
                </div>
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col-lg-12">
                    <h2>Upload Image</h2>


                    <div class="table-responsive">
                        <h3>Your file was successfully uploaded!</h3>

                       <?php 
                       /*
                       <ul>
                            <?php foreach ($upload_data as $item => $value): ?>
                                <li><?php echo $item; ?>: <?php echo $value; ?></li>
                            <?php endforeach; ?>
                    </ul>
                    */?>
                        
                        
                        <div class="col-lg-2">
                            <?php if(is_array($error)){
                            if(!$error['status']){
                                echo "<p class='alert alert-danger'>".$error['msg']."</p>";
                            }else{
                                echo "<p class='alert alert-success'>".$error['msg']."</p>";
                            }
                        } 
                        ?>
                        </div>

                        <p><?php echo anchor('login/gallery', 'Upload Gallery File!'); ?></p>
                    </div>
                </div>

            </div>
            <!-- /.row -->


        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->