<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mission extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user_model');
    }

    public function _remap($method, $params = array()) {
        $method = 'mission_' . $method;
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        }
        show_404();
    }

    public function mission_view() {
        $data['css_path'] = $this->config->item('css');
        $data['js_path'] = $this->config->item('js');
        $data['image_path'] = $this->config->item('image');
        $data['title'] = "Mission Home Page";
        $this->load->view('include/header', $data);
        $this->load->view('mission_view/index', $data);
        $this->load->view('include/footer');
    }

	 // this function is used for filtering purpose
    public function mission_upload() {
        $data['css_path'] = $this->config->item('css');
        $data['js_path'] = $this->config->item('js');
        $data['image_path'] = $this->config->item('image');
        $data['title'] = "Mission Image Upload Page";
        $this->load->view('include/header', $data);
        $this->load->view('mission_view/upload', $data);
        $this->load->view('include/footer');
    }
	
    public function mission_jury() {
        $data['css_path'] = $this->config->item('css');
        $data['js_path'] = $this->config->item('js');
        $data['image_path'] = $this->config->item('image');
        $data['title'] = "Mission Jury Page";
        $this->load->view('include/header', $data);
        $this->load->view('mission_view/jury', $data);
        $this->load->view('include/footer');
    }

    public function mission_prize() {
        $data['css_path'] = $this->config->item('css');
        $data['js_path'] = $this->config->item('js');
        $data['image_path'] = $this->config->item('image');
        $data['title'] = "Mission Prize Page";
        $this->load->view('include/header', $data);
        $this->load->view('mission_view/prize', $data);
        $this->load->view('include/footer');
    }

    public function mission_thankyou() {
        $data['css_path'] = $this->config->item('css');
        $data['js_path'] = $this->config->item('js');
        $data['image_path'] = $this->config->item('image');
        $data['title'] = "Mission Thankyou Page";
        $this->load->view('include/header', $data);
        $this->load->view('mission_view/thankyou', $data);
        $this->load->view('include/footer');
    }

    public function mission_login() {
        $data['css_path'] = $this->config->item('css');
        $data['js_path'] = $this->config->item('js');
        $data['image_path'] = $this->config->item('image');
        $data['title'] = "Mission Login Page";
        $this->load->view('include/header', $data);
        $this->load->view('mission_view/login', $data);
        $this->load->view('include/footer');
    }

    public function mahindra_export() {
        $posted_data = array_map(array($this, "mahindra_validate"), array($this->input->post('id')));
//        $posted_data = $this->input->post('id');
//        $posted_data = "1,2";
        $table = "mojoForm";
        $select = array("id", "name", "email", "pno", "city");
        $condition = explode(",", $posted_data);
        $is_row = 0;
        $data['record'] = $this->user_model->get_in($table, $select, $condition, $is_row);

        if (is_array($data['record'])) {
            $success['result'] = array(
                "success" => 1,
                "msg" => "Data is Downloading",
                "data" => $data['record'],
            );
        } else {
            $success['result'] = array(
                "success" => 0,
                "msg" => "Error",
                "data" => $data['record'],
            );
        }

        $this->load->view("json/reponse", $success);
    }

    //gallery for mahindra
    public function mahindra_gallery() {
        //$posted_data = array_map(array($this, "mahindra_validate"), array($this->input->post()));

        $data['css_path'] = $this->config->item('css');
        $data['js_path'] = $this->config->item('js');
        $data['title'] = "Upload photo for gallery";
        $data['error'] = '';
        $this->load->view('include/header', $data);
        $this->load->view('mahindra_view/gallery', $data);
        $this->load->view('include/footer');
    }

    //code for uploading image
    public function mahindra_do_upload() {
        $data['css_path'] = $this->config->item('css');
        $data['js_path'] = $this->config->item('js');
        $data['title'] = "suceessfully upload gallery page";

        if ($this->input->post('image_submit')) {
            $config['allowed_types'] = 'jpeg|jpg|png';
            $config['max_size'] = '1024';
            $config['max_width'] = '1280';
            $config['max_height'] = '768';
            $config['encrypt_name'] = TRUE;
            $config['remove_spaces'] = TRUE;
            $config['upload_path'] = './uploads';
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload()) {
                $data['error'] = array(
                    'status' => 0,
                    'msg' => $this->upload->display_errors()
                );
                $this->load->view('include/header', $data);
                $this->load->view('mahindra_view/gallery', $data);
                $this->load->view('include/footer');
            } else {
                unset($config);
                $data['upload_data'] = $this->upload->data();
                $full_image_path = $data['upload_data']['full_path'];

                //code for image resize
                $config['image_library'] = 'gd2';
                $config['source_image'] = $full_image_path;
                $config['create_thumb'] = TRUE;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 300;
                $config['height'] = 270;

                $this->load->library('image_lib', $config);

                if (!$this->image_lib->resize()) {
                    $data['error'] = array(
                        'status' => 0,
                        'msg' => $this->image_lib->display_errors()
                    );
                } else {
                    $thumb_explode = explode(".", $full_image_path);
                    $thumb_path = $thumb_explode[0] . "_thumb" . $thumb_explode[1];
                    $insert_data = array(
                        'image' => $full_image_path,
                        'thumb_image' => $thumb_path,
                        'type' => 'image'
                    );
                    $table = "gallery";
                    $data['record'] = $this->user_model->insert($table, $insert_data);
                    if ($data['record']) {
                        $data['error'] = array(
                            'status' => 1,
                            'msg' => 'Image and thumbnail image Created successfully'
                        );
                        //end of code for image resize
                        $this->load->view('include/header', $data);
                        $this->load->view('mahindra_view/gallery_success', $data);
                        $this->load->view('include/footer');
                    } else {
                        $data['error'] = array(
                            'status' => 0,
                            'msg' => 'Unknoen errror please try again later'
                        );
                    }
                }
            }
        } else {


            $config['allowed_types'] = 'jpeg|jpg|png';
            $config['max_size'] = '1024';
            $config['max_width'] = '300';
            $config['max_height'] = '270';
            $config['encrypt_name'] = TRUE;
            $config['remove_spaces'] = TRUE;
            $config['upload_path'] = './uploads';
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload()) {
                $data['error'] = array(
                    'status' => 0,
                    'msg' => $this->upload->display_errors()
                );
                $this->load->view('include/header', $data);
                $this->load->view('mahindra_view/gallery', $data);
                $this->load->view('include/footer');
            } else {
                $data['upload_data'] = $this->upload->data();
                $thumb_path = $data['upload_data']['full_path'];

                $insert_data = array(
                    'image' => $this->input->post('url'),
                    'thumb_image' => $thumb_path,
                    'type' => 'video'
                );
                $table = "gallery";
                $data['record'] = $this->user_model->insert($table, $insert_data);
                if ($data['record']) {
                    $data['error'] = array(
                        'status' => 1,
                        'msg' => 'Video and thumbnail image uploaded successfully'
                    );
                    //end of code for image resize
                    $this->load->view('include/header', $data);
                    $this->load->view('mahindra_view/gallery_success', $data);
                    $this->load->view('include/footer');
                } else {
                    $data['error'] = array(
                        'status' => 0,
                        'msg' => 'Unknoen errror please try again later'
                    );
                }
            }
        }
        unset($_POST);
        unset($_FILES);
    }

}
