<?php

//defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('api_model');
    }

    public function _remap($method, $params = array()) {
        $method = 'api_' . $method;
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        }
        show_404();
    }

    public function api_dealer() {

        $table = "dealership";
        $select = array("mno", "email", "address", "name", "city", "pin");
        $condition = array("city" => $this->input->post('city'));
        $is_row = 0;
        $dealer['result'] = $this->api_model->get($table, $select, $condition, $is_row);
        $this->load->view("json/reponse", $dealer);
    }

    public function api_gallery() {
        $table = "gallery";
        $select = array("image", "thumb_image");
        $condition = array("type" => $this->input->post('type'));
        $is_row = 0;
        $dealer['result'] = $this->api_model->get($table, $select, $condition, $is_row);
        $this->load->view("json/reponse", $dealer);
    }

}
