<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        }

    
    public function get($table,$select,$condition,$is_row){
        $this->db->select($select);
        $this->db->where($condition);
        $query = $this->db->get($table);
        if($is_row){
            return $query->row_array();
        }else{
            return $query->result_array();
        }
        
    }
}
