<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        }

        public function check_login($table,$check){
        $sql = "SELECT * FROM $table WHERE email = ? AND password = ?";
        $query = $this->db->query($sql, $check);
        if($query->num_rows() > 0){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    public function get($table,$select,$condition,$is_row){
        $this->db->select($select);
        $query = $this->db->get_where($table,$condition);
        if($is_row){
            return $query->row_array();
        }else{
            return $query->result_array();
        }
        
    }
    
    public function get_in($table,$select,$condition,$is_row){
        $this->db->select($select);
        $this->db->from($table);
        $this->db->where_in('id', $condition);
        $query = $this->db->get();
        if($is_row){
            if($query->num_rows() > 0){
            return $query->row_array();
            }else{
                return false;
            }
        }else{
            if($query->num_rows() > 0){
            return $query->result_array();
            }else{
                return false;
            }
        }
        
    }
    
    // function for inserting data into the table
    public function insert($table,$data){
        $this->db->insert($table, $data);
        if($this->db->affected_rows()){
            return true;
        }else{
            return false;
        }
    }
    
    

}
